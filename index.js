const log4js = require('log4js');

const logger = log4js.getLogger();
logger.level = "debug";

logger.debug("Iniciando aplicación en modo de pruebas.");
logger.info("La app ha iniciado correctamente.");
logger.warn("Falta el archivo config de la app.");
logger.error("No se pudo acceder al sistema de archivos del equipo.");
logger.fatal("Aplicación no se pudo ejecutar en el sistema operativo.");

// libreria mocha en la carpeta del proyecto = npm i --save-dev mocha

let Hola_lint = 0;  
function sumar(x,y){
    return x + y;    
}

module.exports = sumar;

// Entregar archivo .readme

// Hacer un git init

// carpeta de proyecto

// .eslint
// .gitingnore
// index
// package.json